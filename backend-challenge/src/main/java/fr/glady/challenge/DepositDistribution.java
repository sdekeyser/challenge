package fr.glady.challenge;

import fr.glady.challenge.exception.CompanyBalanceTooLowException;
import fr.glady.challenge.model.DepositTypeEnum;
import fr.glady.challenge.model.User;

public interface DepositDistribution {

    void distributeDeposit(User user, int value, DepositTypeEnum type) throws CompanyBalanceTooLowException;

    int getUserBalance(User user);
}
