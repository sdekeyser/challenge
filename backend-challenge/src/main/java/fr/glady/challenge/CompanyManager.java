package fr.glady.challenge;

public interface CompanyManager {

    void setBalance(int balance);

    int getBalance();

}
