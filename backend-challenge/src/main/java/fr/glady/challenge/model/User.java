package fr.glady.challenge.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class User {

    private List<Deposit> deposits = new ArrayList<>();

    public List<Deposit> getDeposits() {
        return deposits;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }

    public List<Deposit> getDeposits(DepositTypeEnum type) {
        return deposits.stream().filter(deposit -> type.equals(deposit.getType())).collect(Collectors.toList());
    }

    public void addDeposit(int value, DepositTypeEnum type, LocalDate date) {
        deposits.add(new Deposit(type, value, date));
    }
}
