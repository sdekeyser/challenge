package fr.glady.challenge.model;

import java.time.LocalDate;

public class Deposit {

    private DepositTypeEnum type;
    private int value;
    private LocalDate expirationDate;

    public Deposit(DepositTypeEnum type, int value, LocalDate expirationDate) {
        this.type = type;
        this.value = value;
        this.expirationDate = expirationDate;
    }

    public DepositTypeEnum getType() {
        return type;
    }

    public void setType(DepositTypeEnum type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }
}
