package fr.glady.challenge;

public class CompanyManagerImpl implements CompanyManager {

    private int balance = 0;

    @Override
    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public int getBalance() {
        return balance;
    }
}
