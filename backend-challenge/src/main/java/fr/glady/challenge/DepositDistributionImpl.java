package fr.glady.challenge;

import fr.glady.challenge.exception.CompanyBalanceTooLowException;
import fr.glady.challenge.model.Deposit;
import fr.glady.challenge.model.DepositTypeEnum;
import fr.glady.challenge.model.User;

import java.time.LocalDate;

public class DepositDistributionImpl implements DepositDistribution {

    private final CompanyManager companyManager;

    public DepositDistributionImpl(CompanyManager companyManager) {
        this.companyManager = companyManager;
    }

    @Override
    public void distributeDeposit(User user, int value, DepositTypeEnum type) throws CompanyBalanceTooLowException {
        if (companyManager.getBalance() < value) {
            throw new CompanyBalanceTooLowException();
        } else {
            final LocalDate now = LocalDate.now();
            LocalDate expirationDate = type.equals(DepositTypeEnum.GIFT) ? now.plusYears(1) : now.withMonth(2).plusYears(1);
            user.addDeposit(value, type, expirationDate);
        }
    }

    @Override
    public int getUserBalance(User user) {
        return user.getDeposits().stream().filter(deposit -> LocalDate.now().isBefore(deposit.getExpirationDate())).mapToInt(Deposit::getValue).sum();
    }
}
