package fr.glady.challenge;

import fr.glady.challenge.exception.CompanyBalanceTooLowException;
import fr.glady.challenge.model.DepositTypeEnum;
import fr.glady.challenge.model.User;
import org.assertj.core.api.Assertions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;

public class DepositDistributionTest {

    private DepositDistribution depositDistribution;
    private CompanyManager companyManager;

    @BeforeMethod
    public void initDepositDistributionTest() {
        companyManager = new CompanyManagerImpl();
        companyManager.setBalance(1000);
        depositDistribution = new DepositDistributionImpl(companyManager);
    }

    @Test
    public void distributeDepositShouldAddDepositToUser() throws CompanyBalanceTooLowException {
        User user = new User();
        int value = 100;
        DepositTypeEnum type = DepositTypeEnum.GIFT;

        depositDistribution.distributeDeposit(user, value, type);

        Assertions.assertThat(user.getDeposits()).hasSize(1);
    }

    @Test
    public void distributeGiftDepositShouldAddGiftDepositToUser() throws CompanyBalanceTooLowException {
        User user = new User();
        int value = 100;
        DepositTypeEnum type = DepositTypeEnum.GIFT;

        depositDistribution.distributeDeposit(user, value, type);

        Assertions.assertThat(user.getDeposits(DepositTypeEnum.GIFT)).hasSize(1);
    }

    @Test
    public void distributeMealDepositShouldAddMealDepositToUser() throws CompanyBalanceTooLowException {
        User user = new User();
        int value = 100;
        DepositTypeEnum type = DepositTypeEnum.MEAL;

        depositDistribution.distributeDeposit(user, value, type);

        Assertions.assertThat(user.getDeposits(DepositTypeEnum.MEAL)).hasSize(1);
    }

    @Test
    public void distributeDepositShouldThrowIfCompanyBalanceTooLow() {
        User user = new User();
        int value = 100;
        DepositTypeEnum type = DepositTypeEnum.MEAL;
        companyManager.setBalance(50);

        Assertions.assertThatThrownBy(() -> depositDistribution.distributeDeposit(user, value, type)).isInstanceOf(CompanyBalanceTooLowException.class);
    }

    @Test
    public void getUserBalance() throws CompanyBalanceTooLowException {
        User user = new User();
        final int mealDeposit = 100;
        depositDistribution.distributeDeposit(user, mealDeposit, DepositTypeEnum.MEAL);
        final int giftDeposit = 50;
        depositDistribution.distributeDeposit(user, giftDeposit, DepositTypeEnum.GIFT);

        final int userBalance = depositDistribution.getUserBalance(user);

        Assertions.assertThat(userBalance).isEqualTo(mealDeposit + giftDeposit);
    }

    @Test
    public void getUserBalanceShouldNotConsiderExpiredGiftDeposits() throws CompanyBalanceTooLowException {
        User user = new User();
        final int mealDeposit = 100;
        depositDistribution.distributeDeposit(user, mealDeposit, DepositTypeEnum.MEAL);
        final int giftDeposit = 50;
        depositDistribution.distributeDeposit(user, giftDeposit, DepositTypeEnum.GIFT);
        user.getDeposits(DepositTypeEnum.GIFT).stream().findFirst().ifPresent(deposit -> deposit.setExpirationDate(LocalDate.now().minusDays(1)));

        final int userBalance = depositDistribution.getUserBalance(user);

        Assertions.assertThat(userBalance).isEqualTo(mealDeposit);
    }

    @Test
    public void getUserBalanceShouldNotConsiderExpiredMealDeposits() throws CompanyBalanceTooLowException {
        User user = new User();
        final int mealDeposit = 100;
        depositDistribution.distributeDeposit(user, mealDeposit, DepositTypeEnum.MEAL);
        user.getDeposits(DepositTypeEnum.MEAL).stream().findFirst().ifPresent(deposit -> deposit.setExpirationDate(LocalDate.now().minusDays(1)));
        final int giftDeposit = 50;
        depositDistribution.distributeDeposit(user, giftDeposit, DepositTypeEnum.GIFT);

        final int userBalance = depositDistribution.getUserBalance(user);

        Assertions.assertThat(userBalance).isEqualTo(giftDeposit);
    }

    @Test
    public void distributeGiftDepositShouldSetExpirationDateToNextYear() throws CompanyBalanceTooLowException {
        User user = new User();
        int value = 100;
        DepositTypeEnum type = DepositTypeEnum.GIFT;

        depositDistribution.distributeDeposit(user, value, type);

        Assertions.assertThat(user.getDeposits(DepositTypeEnum.GIFT)).allMatch(deposit -> deposit.getExpirationDate().isEqual(LocalDate.now().plusYears(1)));
    }

    @Test
    public void distributeMealDepositShouldSetExpirationDateToNextYearFebruary() throws CompanyBalanceTooLowException {
        User user = new User();
        int value = 100;
        DepositTypeEnum type = DepositTypeEnum.MEAL;

        depositDistribution.distributeDeposit(user, value, type);

        Assertions.assertThat(user.getDeposits(DepositTypeEnum.MEAL)).allMatch(deposit -> deposit.getExpirationDate().isEqual(LocalDate.now().withMonth(2).plusYears(1)));
    }
}
